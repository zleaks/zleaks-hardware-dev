#include <RH_ASK.h>
#include <SPI.h>

float sensorData = 0;

RH_ASK driver;

void setup()
{
  pinMode(13, OUTPUT);
  Serial.begin(115200);
  if (!driver.init())
     digitalWrite(13, HIGH);
}

void loop()
{
    uint8_t buf[12];
    uint8_t buflen = sizeof(buf);
    if (driver.recv(buf, &buflen)) // Non-blocking
    {
      digitalWrite(13, HIGH);
      // Message with a good checksum received, dump it.
      Serial.print((char*)buf);
      digitalWrite(13, LOW);
    }
}
