#include <RH_ASK.h>
#include <SPI.h> // Not actually used but needed to compile

#define FLOWPIN 3
#define DEVICEID "02"

int DELAY_TIMER;
int delay_time;
int BUFFER_COUNTER;
int COUNT;
double FLOW_BUFFER[3] = {0.0, 0.0, 0.0};
char TX_BUFFER[100];

RH_ASK driver;

void edgeCount()
{
  COUNT += 1;
  return;
}

double getFlow()  //entrega l/min
{
  double f;
  int c = COUNT;
  COUNT = 0;
  f = c / 5.5;
  
  return f;
}

void setup()
{
  pinMode(13, OUTPUT);
    Serial.begin(9600);   // Debugging only
    if (!driver.init())
         Serial.println("init failed");
         
  attachInterrupt(FLOWPIN, edgeCount, RISING);
  delay_time = 0;
  BUFFER_COUNTER = 0;
  COUNT = 0;
  DELAY_TIMER = millis();
  
}

void loop()
{
  delay_time = (1000-(millis()-DELAY_TIMER));

  for(int i = 0; i < 3; i++)
  {
    if(delay_time > 0)
      delay(delay_time);
    
    DELAY_TIMER = millis();
  
    FLOW_BUFFER[i] = getFlow();
  }

  sprintf(TX_BUFFER, "<%s : %2.2f-%2.2f-%2.2f>", DEVICEID, FLOW_BUFFER[0], FLOW_BUFFER[1], FLOW_BUFFER[2]);
  digitalWrite(13, HIGH);
  driver.send((uint8_t *)TX_BUFFER, strlen(TX_BUFFER));
  driver.waitPacketSent();
  digitalWrite(13, LOW);
}
