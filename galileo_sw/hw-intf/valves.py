from gpiocnf import gpio
VALVE0 = -1
STATUS_VALVE0 = 1	#1 - OPENED, 0 - CLOSED

VALVE1 = -1
STATUS_VALVE1 = 0
	
#### CLOSE/OPEN VALVES	####


def openValve(pin):
	gpio.digitalWrite(pin, gpio.HIGH)

def closeValve(pin):
	gpio.digitalWrite(pin, gpio.LOW)
	
def initValve(pin):
	global VALVE0
	global VALVE1
	gpio.pinMode(pin, gpio.OUTPUT)
	if(VALVE0 == -1):
		VALVE0 = pin
	else:
		VALVE1 = pin

	openValve(pin)
	print("Valve ~ OK.")
	
def modifyValve():
	global STATUS_VALVE0
	
	if(STATUS_VALVE0 == 1):
		closeValve(5)
		STATUS_VALVE0 = 0
		
	else:
		openValve(5)
		STATUS_VALVE0 = 1

#def solSwitching(flow):



############################
