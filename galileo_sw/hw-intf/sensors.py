from gpiocnf import gpio
import threading
import time
import datetime

#IN GALILEO, ANALOG PINS RANGE: 14~19 (A0~A5)
#USE 14~19 TO READ FROM PRESSURE SENSOR
#ARDUPIN = 11	#arduino bit pin wich switchs from wich file read

FLOW = None
PRESSURE = None
RAW = None
H = 0
T = 0
	
#### FLOW, HUMIDITY AND TEMPERATURE FUNCTIONS	####

def getRawData(d_pin):	#Collect the sensors data from the arduino's file
	stop = False
	f_data = 0
	while not stop:
		if( gpio.digitalRead(d_pin)  == 0 ):
			stop = True
			f = open('hw-intf/buffers/sensors.out')
			f_data = f.read()
			f.close()
			f_data = f_data[0:len(f_data)-2]
		else:
			print("File busy...")
			time.sleep(0.1)
			
	return f_data

	
def rawHandler(d_pin):
	global RAW
	while True:
		RAW = getRawData(d_pin)
		#print(RAW)
		
		RAW = RAW.split(' ')
		for i in range (0,3):
			RAW[i] = RAW[i].split('-')
		time.sleep(2)
	
def getDHTData():	#returns temperature and humidity
	global H
	global T
	
	while RAW == None:
		time.sleep(0.5)

	H = [float(RAW[0][0]), float(RAW[1][0]), float(RAW[2][0])]
	T = [float(RAW[0][1]), float(RAW[1][1]), float(RAW[2][1])]
	return H, T

	
def getFlowData():
	global FLOW
	global RAW
	
	while RAW == None:
		time.sleep(0.5)
		
	FLOW = [ float(RAW[0][2]), float(RAW[1][2]), float(RAW[2][2])]
	return FLOW

def arduConfig(ardupin):
	gpio.pinMode(ardupin, gpio.INPUT_PULLDOWN)
	t = threading.Thread(target = rawHandler, args=(ardupin, ))
	t.start()
	
	
####################################################

	
	

############## PRESSURE FUNCTIONS	################

def avgFIR(FIR):
	avg = 0
	for i in range(len(FIR)):
		avg += FIR[i]
	avg /= len(FIR)
	return avg

def calcPressure(pFIR, pZoffset):
	sample = avgFIR(pFIR)
	return (((sample - pZoffset) * 0.007161458) + 100)
	

def millis():
	return int(round(time.time() * 1000))

def refreshPressure(pin, pZoffset):
	p = [0, 0, 0]
	pFIR = list(range(150))
	indexFIR = 0
	timer = millis()
	i = 0

	while(True):
		gpio.analogRead(pin)
		pFIR[indexFIR] = gpio.analogRead(pin)
		indexFIR = (indexFIR + 1) % 150

		if(millis() > timer):
			p[i] = calcPressure(pFIR, pZoffset)
			i = i + 1
			
			if i > 2:
				i = 0
				PRESSURE = p
				
			timer += 1000
	

def pressureSetup(pin, pZoffset):
	gpio.pinMode(pin, gpio.ANALOG_INPUT)
	t = threading.Thread(target = refreshPressure, args=(pin, pZoffset, ))
	t.start()


def getPressureData():
	global PRESSURE
	while PRESSURE == None:
		time.sleep(0.5)
		
	return PRESSURE

####################################################

def getAllData():	#returns in order: (HOUR, [HUMIDITY, TEMPERATURE, FLOW, PRESSURE])
	global PRESSURE
	PRESSURE = [0, 0, 0]
	time = datetime.datetime.now().ctime()
	data = [getDHTData(), getFlowData(), getPressureData()]
	
	return time, data

	
def initSensors(ardupin, pressurepin):
	arduConfig(ardupin)
	print("Arduino side ~ OK.")
	pressureSetup(pressurepin, 0)
	print("Pressure sensor ~ OK.")
	time.sleep(3)