import datetime, time
import httplib, urllib, json

SERVER = "192.168.43.225:9000" #trocar nome do servidor

#enviar consumo em litros

def getTime():
    locTime = time.localtime()
    stri =  str(locTime.tm_year) + "-" + str(locTime.tm_mon) + "-" + str(locTime.tm_mday) + " " + str(locTime.tm_hour) + ":" + str(locTime.tm_min) + ":" + str(locTime.tm_sec)
    return stri   

	
def sendConsumption(f, ID):
	headers = {"Content-type": "application/x-www-form-urlencoded",
				"Accept": "text/plain"}
	conn = httplib.HTTPConnection(SERVER)
	
	try:
		conn.request("POST", "/api/waste", urllib.urlencode({	'device': ID,
																'waste': f, 
																'time' : getTime()}), headers)
		#print(str(urllib.urlencode({'waste': f, 'time' : getTime()})))
		
	except:
		print("Consumption not send, trying again later.")
		conn.close()
		return False
		
	else:
		print("Consumption sent")
		conn.close()
		return True
	

def comServ(p, f, t, h, ID):
	ret = 0
	p = str(p)
	t = str(t)
	h = str(h)
	f = str(f)
	
	p = p[1:len(p)-1]
	t = t[1:len(t)-1]
	h = h[1:len(h)-1]
	f = f[1:len(f)-1]
	
	params = urllib.urlencode({'device': ID, 
								'pressure': p,
								'temp' : t,
								'humidity' : h,
								'flux' : f,
								'time' : getTime()})

	headers = {"Content-type": "application/x-www-form-urlencoded",
				"Accept": "text/plain"}

	conn = httplib.HTTPConnection(SERVER)
	try:
		conn.request("GET", "/api/valve")
		
	except:
		print("Server is not online. Data will be sent later.")
		#SALVA DADOS PARA SEREM ENVIADOS DEPOIS
		
	else:
		response = conn.getresponse()
		ret = response.read()
		if(ret == 'true'):
			ret = True
		else:
			ret = False
		conn.request("POST", "/api/sensordata", params, headers)
		print("Data successfully sent.")
		
	conn.close()
	return ret
	

def sendLeak(leak):
	conn = httplib.HTTPConnection(SERVER)
	try:
		conn.request("POST", "/api/leak", urllib.urlencode({'leak': leak}))
	
	except:
		conn.close()
		return False
		
	else:
		conn.close()
		return True
		
def getResponseLeak():
	conn = httplib.HTTPConnection(SERVER)
	try:
		conn.request("GET", "/api/respLeak")
	
	except:
		conn.close()
		return [False, False, False]
		
	else:
		conn.close()
		response = conn.getresponse()
		ret = response.read()
		return [True, ret['vazando'], ret['frequente']]
		
#def connHandler(conn, par0, par1):
#	try:
#		conn.request(par0, par1)
#	except:
#		print("Server is not online. Data will be sent later.")
#		return False
#	else
#		if(par0 == "GET"):
#		
#		elif(par0 == "POST"):
