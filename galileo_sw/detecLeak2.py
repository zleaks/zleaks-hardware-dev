import sensors
import httplib

END_DAY = '21:0:0'
BOTTOM_PEAK_BEG = '6:0:0'
BOTTOM_PEAK_END = '6:20:0'
TOP_PEAK_BEG = '14:0:0'
TOP_PEAK_END = '14:20:0'
VAZAMENTO = False;

def get_sec(s):
	l = s.split(':')
	return int(l[0])*3600 + int(l[1])*60 + int(l[2])


def checkLeak():
	time, data = sensors.getAllData()
	#print(sensors.getAllData())
	time = time.split(' ')
	#date = time[0]+time[1]+time[2]
	
	if(time[2] == ''):
		hour = time[4]
	else:
		hour = time[3]

#	print("Flow: " + str(data[1][2]))
	
	seconds = get_sec(hour)

	if (seconds > get_sec(END_DAY) or seconds < get_sec(BOTTOM_PEAK_BEG)) and (data[1][0] > 0 or data[1][1] > 0 or data[1][2] > 0):
		VAZAMENTO = True
		print("1")
	elif data[1][0] > 2 or data[1][1] > 2 or data[1][2] > 2:
		VAZAMENTO = True
		print("2")
	elif (((seconds > get_sec(BOTTOM_PEAK_END) and seconds < get_sec(TOP_PEAK_BEG)) or (seconds > get_sec(TOP_PEAK_END) and seconds < get_sec(END_DAY))) and (data[1][0] > 1.3 or data[1][1] > 1.3 or data[1][2] > 1.3)):
		VAZAMENTO = True
		print("3")

	else: VAZAMENTO = False
	
	return VAZAMENTO
