import os, datetime, time, sys
import httplib, urllib, json

PATH = os.getcwd() + '/hw-intf/'
sys.path.insert(0, PATH)
import sensors, detecLeak, detecLeak2, com, valves, radio

ACUMULATED_FLUX = 0
#ACUMULATED_FLUX_LEAKS = 0
#__waiting_response_leak = False
__time_updated = False
__mod_valve = 0
__leaking = False
__send_leak = False
__leak_sent = False

def getActualSecs():
	s = str(datetime.datetime.now().time())
	s = s.split('.')[0]
	actual = detecLeak.get_sec(s)
	return actual


#	SYSTEM INITIALIZES	#
def updateSysTime():
	print("Trying to connect to the server...")
	conn = httplib.HTTPConnection(com.SERVER)
	
	try:
		conn.request("GET", "/api/time") 
		
	except:
		print("Server is not online. Time sync will retry later.")
		
	else:
		response = conn.getresponse()
		data1 = response.read()
		data = json.loads(data1)
		

		os.system("timedatectl set-ntp 0")
		os.system("timedatectl set-time '" + str(data['year']) + "-" + str(data['month']) + "-" + str(data['day']) + " " + str(data['hour']) + ":" + str(data['minute']) + ":" + str(data['second']) + "'")
		print("Time sync completed.")
		__time_updated = True
		
	conn.close()
#########################

ACTUAL_TIME = getActualSecs()
SEND_TIME1 = detecLeak.get_sec('0:00:00')
SEND_TIME2 = detecLeak.get_sec('8:00:00')
SEND_TIME3 = detecLeak.get_sec('16:00:00')

SEND_FLAG1 = False
SEND_FLAG2 = False
SEND_FLAG3 = False


if ACTUAL_TIME > SEND_TIME3:
	SEND_FLAG1 = True
	
elif ACTUAL_TIME > SEND_TIME2:
	SEND_FLAG3 = True
	
elif ACTUAL_TIME > SEND_TIME1:
	SEND_FLAG2 = True	
	
	
	
#INITIALIZES SYSTEM

sensors.initSensors(11, 14)
valves.initValve(5)
valves.initValve(6)
valves.closeValve(6)
updateSysTime()
count_leak_detect = sensors.millis()
count_acumulate_flux = sensors.millis()
count_send_consup = sensors.millis()

print("~ZLeaks online~")

while True:

	actual_time = getActualSecs()
	
	if sensors.millis()- count_send_consup >= 60000:
		__consumption_sent =  com.sendConsumption(ACUMULATED_FLUX, 1)
		count_send_consup = sensors.millis()
		
		if __consumption_sent:
			ACUMULATED_FLUX = 0
			
		#if __waiting_response_leak:
			#ans = getResponseLeak()
			#if ans[0] == True:
				#detecLeak.processAnswer(ans[1], ans[2])
				#__waiting_response_leak = False
	
	if sensors.millis() - count_leak_detect >= 3000: #checa vazamento
	
		if not __time_updated:
			updateSysTime()
			
		count_leak_detect = sensors.millis()
		#__waiting_response_leak = False
		
		#print(sensors.RAW)
		__leaking = detecLeak2.checkLeak()
		print("Vazamento foi enviado? " + str(__leak_sent))
		#ACUMULATED_FLUX_LEAKS = 0
		
		if __leaking:
			print("Vazamento detectado...")
			__send_leak = True
			#__waiting_response_leak = True
			
		else:
			print("Nao tem vazamento.")
			__leak_sent = False
			__send_leak = False
		
		if __send_leak and not __leak_sent:
			response = com.sendLeak(__send_leak)
			print("Vazamento enviado.")
#			print("response" + str(response))
			if response:
				__send_leak = False
				__leak_sent = True
			
		
		print("\n")
		print("Trying to send data...")
		print(str(sensors.PRESSURE)+str(sensors.FLOW)+str(sensors.T)+str(sensors.H))

		ACUMULATED_FLUX = ACUMULATED_FLUX+sensors.FLOW[0]/60+sensors.FLOW[1]/60+sensors.FLOW[2]/60
		#ACUMULATED_FLUX_LEAKS = ACUMULATED_FLUX_LEAKS+sensors.FLOW[0]/60+sensors.FLOW[1]/60+sensors.FLOW[2]/60
		__mod_valve = com.comServ(sensors.PRESSURE, sensors.FLOW, sensors.T, sensors.H, 1)

#		dev_data = radio.getData()	#SO LE APENAS 1 NO POR VEZ
#		com.comServ(0, dev_data(1), 0, 0, dev_data(0))
		print("Fluxo acumulado: " + str(ACUMULATED_FLUX))
		
	if __mod_valve == True:
		valves.modifyValve()
		__mod_valve = False
		
		
		





		
		
		
		
		
		
	#if actual_time > SEND_TIME3:
		#if SEND_FLAG3 == True:	#SEND THE 3RD PACKAGE OF DATA
		#	SEND_FLAG3 = False
		#	SEND_FLAG1 = True
		#	#SEND DATA TO SERVER
			
	#elif actual_time > SEND_TIME2:
	#	if SEND_FLAG2 == True:	#SEND THE 2ND PACKAGE OF DATA
	#		SEND_FLAG2 = False
	#		SEND_FLAG3 = True
	#		#SEND DATA TO SERVER
			
	#elif actual_time > SEND_TIME1:
	#	if SEND_FLAG1 == True:	#SEND THE 1ST PACKAGE OF DATA
	#		SEND_FLAG1 = False
	#		SEND_FLAG2 = True
	#		#SEND DATA TO SERVER
