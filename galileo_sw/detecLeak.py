#import sensors
#import httplib
import time, math

ERRO_TOLERANCIA = 0.5
ERRO = 0
thresholds = [[0, 0, 0, 0, 0, 0, 14, 4, 0, 0, 2, 0, 24, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 0, 0, 0, 0, 14, 4, 0, 0, 2, 0, 24, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 0, 0, 0, 0, 14, 4, 0, 0, 2, 0, 24, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 0, 0, 0, 0, 14, 4, 0, 0, 2, 0, 24, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 0, 0, 0, 0, 14, 4, 0, 0, 2, 0, 24, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 14, 0, 0, 0, 0, 0, 0, 2, 0, 10, 6, 0], [0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 2, 14, 6, 4, 0]]

dias = {'Monday': 1, 'Tuesday': 2, 'Wednesday': 3, 'Thursday': 4, 'Friday': 5, 'Saturday': 6, 'Sunday': 7}

VAZAMENTO = False;

def get_sec(s):
	l = s.split(':')
	return int(l[0])*3600 + int(l[1])*60 + int(l[2])


def checkLeak(acumulated_flux):
	global thresholds, dias, ERRO_TOLERANCIA, ERRO
	hora = ((time.strftime("%I:%M:%S")).split(':'))[0]
	dia = dias[time.strftime("%A")]

	if abs(thresholds[int(dia)-1][int(hora)-1] - acumulated_flux) > 0.5:
		VAZAMENTO = True
		ERRO = abs(thresholds[int(dia)-1][int(hora)-1] - acumulated_flux)

	return VAZAMENTO
	

def processAnswer(vazando, frequente):
	global ERRO
	taxa = 0
	
	hora = ((time.strftime("%I:%M:%S")).split(':'))[0]
	dia = dias[time.strftime("%A")]
	
	if vazando == False:
		if frequente == True:
			taxa = 1
		else:
			taxa = 0.5
	
		threshold[int(dia)-1][int(hora)-1] = threshold[int(dia)-1][int(hora)-1] + ERRO*taxa
	
	return




