#include "DHT.h"
#include <time.h>
#include <string.h>

#define DHTOUT 2
#define DHTIN 3     // what pin we're connected to
#define DHTTYPE DHT11   // DHT 11 
#define FLOWPIN 4
#define PYPIN 12

#define MAXX_BUFFER_SIZE 3

DHT dht(DHTIN, DHTOUT, DHTTYPE);

char OPTION;
float T = 0,H = 0;
int COUNT;
char SENSOR_DATA[200];
char aux[MAXX_BUFFER_SIZE][20];
int DELAY_TIMER;
time_t now = time(0);
char *dt = ctime(&now);
char *PATH = "home/root/ZLeaks/galileo_sw/hw-intf/buffers/";
char *FILE_PATH = "home/root/ZLeaks/galileo_sw/hw-intf/buffers/sensors.out";
int delay_time = 0;


void edgeCount()
{
  COUNT += 1;
  return;
}

double getFlow()  //entrega l/min
{
  double f;
  int c = COUNT;
  COUNT = 0;
  f = c / 5.5;
  
  return f;
}

void returnConnectionInfo()
{
  system("ifconfig > /dev/ttyGS0");
  return;
}

void readDHT()
{
  double h = dht.readHumidity();
  double t = dht.readTemperature(false);

  if(isnan(h) || isnan(t))
  {
    H = H;
    T = T;
  }
  
  else  // se o dado não estiver pronto, continua com os dados anteriores
  {
    H = h;
    T = t;
  }
  return;
}


void setup()
{ 
  COUNT = 0;
  Serial.begin(9600);
  dht.begin();
  attachInterrupt(FLOWPIN, edgeCount, RISING);
  pinMode(PYPIN, OUTPUT);
  

  Serial.println("~~| ZLeaks Debug Mode |~~");
  Serial.println("1: connections info");
  Serial.println("2: debug flow sensor");
  Serial.println("3: debug DHT");
  Serial.println("4: ~none~");
  Serial.println("5: run sensors reads");
}


void loop ()
{
  delay(1000);
  OPTION = Serial.read();
  switch(OPTION)
  {
    case '1': //return connections info
      returnConnectionInfo();
      OPTION = 0;
    break;

    case '2': //debug flow sensor
      Serial.println(getFlow());
      
    break;
      
    case '3': //debug DHT sensor
      readDHT();
      Serial.print("Humidity: ");
      Serial.println(H);
      Serial.print("Temperature: ");
      Serial.println(T);
    break;


    case '5': //start sensor reads
      DELAY_TIMER = millis();
      
      COUNT = 0;
      Serial.println("Runing...");

      system("mkdir /home/root/zleaks-hardware-dev/galileo_sw/hw-intf/buffers");
      system("touch /home/root/zleaks-hardware-dev/galileo_sw/hw-intf/buffers/sensors.out");
      
      while(true)
      {
        for(int i = 0; i < MAXX_BUFFER_SIZE; i++)
        {
          delay_time = (1000-(millis()-DELAY_TIMER));
          
          if( delay_time > 0 && delay_time <= 1000)
            delay(delay_time);  //exact 1s delay
            
          DELAY_TIMER = millis();
          
          readDHT();
          sprintf(aux[i], "%2.2f-%2.2f-%2.2f", H, T, getFlow());
        }
        
        sprintf(SENSOR_DATA, "echo %s %s %s > /home/root/zleaks-hardware-dev/galileo_sw/hw-intf/buffers/sensors.out", aux[0], aux[1], aux[2]);
        
        now = time(0);
        dt = ctime(&now);
        
        Serial.print(dt);
        Serial.println(SENSOR_DATA);
        Serial.print('\n');
        
        digitalWrite(PYPIN, HIGH);
        system(SENSOR_DATA);
        digitalWrite(PYPIN, LOW);
        
        //Serial.println(1000-(millis()-actual_millis));
        //break; 
      }
      
    break;
      
    default: break;
  }
}
